import java.sql.PreparedStatement;

public class Infix2Prefix {
    static int prioritet(Character c) {
        switch (c) {
            case '+':
            case '-':
                return 1;
            case '*':
                return 2;
            case '/':
                return 2;
            case '^':
                return 3;
        }
        return -1;
    }

    static String infix_to_postfix(String izraz) {
        StringBuilder postfix = new StringBuilder();
        LinkedStack<Character> stack = new LinkedStack<Character>();
        for (int i = 0; i < izraz.length(); i++) {
            Character c = izraz.charAt(i);
            System.out.print(" Char:" + c + "      Tekovna sostojba: " + stack + " Rezultat:" + postfix);
            if (Character.isLetterOrDigit(c)) {
                postfix.append(c);
            } else if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                while (stack.peek() != '(' && !stack.isEmpty()) {
                    postfix.append(stack.pop());
                }
                if (stack.isEmpty())
                    return "Invalid data";
                stack.pop();
            } else {
                while (!stack.isEmpty() && prioritet(c) <= prioritet(stack.peek())) {
                    postfix.append(stack.pop());
                }
                stack.push(c);
            }
            System.out.println("\tNova sostojba: " + stack + " Rezultat:" + postfix);
        }
        while (!stack.isEmpty()) {
            if (stack.peek() == '(')
                return "Invalid data";
            postfix.append(stack.pop());
        }
        return postfix.toString();
    }

    static String infix_to_prefix(String izraz) {
        StringBuilder prefix = new StringBuilder();
        LinkedStack<Character> stack = new LinkedStack<Character>();
        for (int i = 0; i < izraz.length(); i++) {
            Character c = izraz.charAt(i);
            System.out.print(" Char:" + c + "      Tekovna sostojba: " + stack + " Rezultat:" + prefix);
            if (Character.isLetterOrDigit(c)) {
//                postfix.append(c);
                stack.push(c);
            } else if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                while (stack.peek() != '(' && !stack.isEmpty()) {
                    prefix.append(stack.pop());
                }
                if (stack.isEmpty())
                    return "Invalid data";
                stack.pop();
            } else {
//                while (!stack.isEmpty() && prioritet(c) <= prioritet(stack.peek())) {
//                    prefix.append(stack.pop());
//                }
                while (!stack.isEmpty() && prioritet(c) <= prioritet(prefix.charAt(prefix.length()-1))) {
//                    prefix.append(stack.pop());
                    stack.push(prefix.charAt(prefix.length()-1));
                    prefix.deleteCharAt(prefix.length()-1);
                }
//                stack.push(c);
                prefix.append(c);
            }
            System.out.println("\tNova sostojba: " + stack + " Rezultat:" + prefix);
        }
        while (!stack.isEmpty()) {
            if (stack.peek() == '(')
                return "Invalid data";
            prefix.append(stack.pop());
        }
        return prefix.toString();
    }

    static String infixToPostfix(String exp) {
        // initializing empty String for result
        String result = new String("");

        // initializing empty stack
        LinkedStack<Character> stack = new LinkedStack<>();

        for (int i = 0; i < exp.length(); ++i) {
            char c = exp.charAt(i);

            // If the scanned character is an
            // operand, add it to output.
            if (Character.isLetterOrDigit(c))
                result += c;

                // If the scanned character is an '(',
                // push it to the stack.
            else if (c == '(')
                stack.push(c);

                //  If the scanned character is an ')',
                // pop and output from the stack
                // until an '(' is encountered.
            else if (c == ')') {
                while (!stack.isEmpty() && stack.peek() != '(')
                    result += stack.pop();
                stack.pop();
            } else // an operator is encountered
            {
                while (!stack.isEmpty() && prioritet(c)
                        <= prioritet(stack.peek())) {

                    result += stack.pop();
                }
                stack.push(c);
            }

        }

        // pop all the operators from the stack
        while (!stack.isEmpty()) {
            if (stack.peek() == '(')
                return "Invalid Expression";
            result += stack.pop();
        }
        return result;
    }

    public static void main(String[] args) {
        String infix = "a+b*(c^d-e)^(f+g*h)-i";
        System.out.println(infix_to_postfix(infix));
//        System.out.println(infix_to_prefix(infix));
//        System.out.println(infixToPostfix(infix));
//        String ocekuvan_rezultat = "a";
//        +
    }
}
