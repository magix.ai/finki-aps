package cas01_voved;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class IntroTips {

    public static void main_old(String[] args) throws Exception {
        int i, j, k;
        // ednostavna upotreba na funkciite za razdeluvanje na klasata StringTokenizer
        // za razdeluvanje na String vo tokeni (zborovi) so vas definiran String sto
        // sodrzi niza od karakteri koi ke bidat delimiteri, na primer prazno mesto,
        // nova linija, interpunkciski znak
        StringTokenizer st = new StringTokenizer("Ova e test", " ");

        System.out.println("Zborovi:");
        while (st.hasMoreTokens()) {
            System.out.println(st.nextToken());
        }

        // site simboli sto gi razdeluvaat tokenite morame da gi definirame kako vtor argument na konstruktorot
        st = new StringTokenizer("Ova e test. ASP, APS, SP i SPA se predmeti na FINKI!\nVtora linija.", " .,!\n");

        System.out.println("Zborovi:");
        while (st.hasMoreTokens()) {
            System.out.println(st.nextToken());
        }

        // konverzija od String vo broj
        String broj = "123";
        int num = Integer.parseInt(broj);
        System.out.println(num);

        // konverzija od broj vo string (niza od karakteri)
        int aInt = 368;
        String str;
        str = Integer.toString(aInt);

        System.out.println("Broj pretstaven kako niza od karakteri: " + str);

        // citanje na linija od standarden vlez i pecatenje na standarden izlez
        System.out.println("Vnesete tekst:");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String text = br.readLine();
        System.out.println(text);

    }

    public static void main(String[] args) throws Exception {
        int[] niza = {1, 5, 3, 4, 7, 8, 2};
        int n = 7;
        int odd = 0;
        int even = 0;
        int cnt = 0;
        Double avg = 0.0;
//        for (int i = 0; i < niza.length; i++) {
//            cnt++;
//            if (niza[i] % 2 == 0) {
//                even++;
//            } else {
//                odd++;
//            }
//            avg += niza[i];
//        }
        for (int number:niza) {
            cnt++;
            if (number % 2 == 0) {
                even++;
            } else {
                odd++;
            }
            avg += number;
        }
        avg /= cnt;
        System.out.println(odd + ";" + even + ";" + cnt + ";" + avg);
    }
}
