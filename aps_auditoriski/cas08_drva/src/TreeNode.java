// Define a generic class called TreeNode. It holds data of a type that implements Comparable.
public class TreeNode<T extends Comparable<T>> {

    // Declare a public variable 'data' to hold the node's data.
    public T data;

    // Declare pointers for the left and right children of the TreeNode.
    public TreeNode<T> left;
    public TreeNode<T> right;

    // Constructor for initializing the TreeNode with data.
    public TreeNode(T data) {
        // Store the given data in this node.
        this.data = data;

        // Initialize the left and right children to null.
        left = null;
        right = null;
    }


    // Override the toString method to convert the TreeNode into a String representation.
    @Override
    public String toString() {
        // Use a StringBuilder to build the output string.
        StringBuilder sb = new StringBuilder();

        // Call the helper function to build the output string.
        toStringHelper(sb, "", "", this);

        // Convert the StringBuilder to String and return it.
        return sb.toString();
    }

    // Helper function that builds the output recursively in a preorder fashion.
    private void toStringHelper(StringBuilder sb, String padding, String pointer, TreeNode<T> node) {
        // Check if the node is not null.
        if (node != null) {
            // Append the current padding and pointer to the StringBuilder.
            sb.append(padding);
            sb.append(pointer);

            // Append the data of the current node.
            sb.append(node.data);

            // Move to the next line.
            sb.append("\n");

            // Prepare the padding for both the left and right children.
            StringBuilder paddingBuilder = new StringBuilder(padding);
            paddingBuilder.append("|  ");
            String paddingForBoth = paddingBuilder.toString();

            // Prepare the pointers for both the left and right children.
            String pointerForRight = "'--";
            String pointerForLeft = (node.right != null) ? "|--" : "'--";

            // Recursively build the string representation for the left subtree.
            toStringHelper(sb, paddingForBoth, pointerForLeft, node.left);

            // Recursively build the string representation for the right subtree.
            toStringHelper(sb, paddingForBoth, pointerForRight, node.right);
        }
    }


    public String toString_space() {
        // Use a StringBuilder to build the output string.
        StringBuilder sb = new StringBuilder();

        // Call the helper function to build the output string.
        toStringHelper_space(sb, this, 0, 7);

        // Convert the StringBuilder to String and return it.
        return sb.toString();
    }

    private void toStringHelper_space(StringBuilder sb, TreeNode<T> node, int space, int count) {

        if (node == null)
            return;

        // to increase the distance between levels
        space += count;

        // print the right child first
        toStringHelper_space(sb, node.right, space, count);

        // print the current node after adding the spaces
        sb.append("\n");
        for (int i = count; i < space; i++)
            sb.append(" ");
        sb.append(node.data + "\n");

        //print the left child
        toStringHelper_space(sb, node.left, space, count);

    }
}
