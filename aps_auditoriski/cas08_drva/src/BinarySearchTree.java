// Define a generic class BinarySearchTree with elements of type T, where T extends Comparable and
// also extends the BinaryTree class because all its functions and properties could be reused

import java.util.List;

public class BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T> {

    public BinarySearchTree() {
        super(); // Explicitly call superclass constructor
    }

    public BinarySearchTree(TreeNode<T> root) {
        super(root);
    }

    // Private function that recursively searches for a node containing a specific element 'x' in the BST
    private TreeNode<T> find(TreeNode<T> node, T x) {
        // If the current node is null, the element 'x' is not present in this branch of the tree.
        if (node == null)
            return null;

        // Compare 'x' with the current node's data to decide whether to search in the left or right subtree.
        if (x.compareTo(node.data) < 0) {
            // 'x' is smaller than the current node's data, so continue searching in the left subtree.
            return find(node.left, x);
        } else if (x.compareTo(node.data) > 0) {
            // 'x' is larger than the current node's data, so continue searching in the right subtree.
            return find(node.right, x);
        } else {
            // 'x' matches the current node's data, so we've found a match.
            return node;
        }
    }

    // Public function to search for an element 'x' in the BST
    public TreeNode<T> find(T x) {
        // Call the private 'find' method starting from the root of the tree.
        return find(this.root, x);
    }

    // Private function to perform the insert operation, starting at node 'node'.
    private TreeNode<T> insert(TreeNode<T> node, T x) {
        // If node is null, create and return a new node with data 'data'.
        if (node == null) {
            return new TreeNode<>(x);
        }

        // Compare the new data with the node's data. If new data is less, insert into the left subtree.
        if (x.compareTo(node.data) < 0) {
            node.left = insert(node.left, x);
        }
        // If new data is greater, insert into the right subtree.
        else if (x.compareTo(node.data) > 0) {
            node.right = insert(node.right, x);
        }

        // Return the node after insertion.
        return node;
    }

    // Public insert function to add a new node with data 'x'.
    public void insert(T x) {
        // Internal function call to perform the insert operation.
        root = insert(root, x);
    }

    // Private function to remove a node with element 'x' from the BST, starting at node 'node'
    private TreeNode<T> remove(TreeNode<T> node, T x) {
        // If the current node is null, the element 'x' is not present in this branch of the tree.
        if (node == null)
            return node; // Item not found; do nothing

        // Compare 'x' with the current node's data to decide whether to search in the left or right subtree.
        if (x.compareTo(node.data) < 0) {
            // 'x' is smaller than the current node's data, so continue searching in the left subtree.
            node.left = remove(node.left, x);
        } else if (x.compareTo(node.data) > 0) {
            // 'x' is larger than the current node's data, so continue searching in the right subtree.
            node.right = remove(node.right, x);
        } else if (node.left != null && node.right != null) { // Two children
            // If the node to remove has two children, replace its data with the smallest element
            // from its right subtree (in-order successor), and then remove the duplicate element.
            node.data = findMin(node.right).data;
            node.right = remove(node.right, node.data);
        } else {
            // If the node has only one child (or no children), return the non-null child,
            // effectively removing the current node from the tree.
            if (node.left != null)
                return node.left;
            else
                return node.right;
        }
        return node; // Return the updated node.
    }

    // Private method to find the minimum element in a subtree rooted at 'node'.
    private TreeNode<T> findMin(TreeNode<T> node) {
        // If the current node is null, the subtree is empty, so return null.
        if (node == null) {
            return null;
        } else if (node.left == null) {
            // If there is no left child, this node contains the minimum element in the subtree.
            return node;
        } else {
            // Continue searching for the minimum element in the left subtree.
            return findMin(node.left);
        }
    }

    // Public remove function to remove a node with data 'x'.
    public void remove(T x) {
        // Internal function call to perform the remove operation.
        root = remove(root, x);
    }

    // This method calculates the height (maximum depth) of a binary tree rooted at 'node'.
    public int height(TreeNode<T> node) {
        // Base case: If the current node is null, the height is 0.
        if (node == null) {
            return 0;
        }
        
        // Recursively calculate the height of the left and right subtrees,
        // and return the maximum height plus 1 (to account for the current node).
        return 1 + Math.max(height(node.left), height(node.right));
    }


    // This method checks whether a binary tree rooted at 'node' is balanced.
    public boolean isBalanced(TreeNode<T> node) {
        int left_h, right_h;
        
        // Base case: If the current node is null, it's considered balanced.
        if (node == null) {
            return true;
        }
        
        // Calculate the heights of the left and right subtrees.
        left_h = height(node.left);
        right_h = height(node.right);
        
        // Check if the difference in heights of left and right subtrees is at most 1,
        // and both left and right subtrees are themselves balanced.
        if (Math.abs(left_h - right_h) <= 1 && isBalanced(node.left) && isBalanced(node.right)) {
            return true; // The tree rooted at 'node' is balanced.
        }
        
        return false; // The tree rooted at 'node' is not balanced.
    }


    // Private function for performing an inorder traversal and store the data in a list
    private void inorder(TreeNode<T> node, List<T> sortedInorder) {
        // Base case: If the current node is not null.
        if (node != null) {
            // Recursively traverse the left subtree.
            inorder(node.left, sortedInorder);
            
            // Add the data of the current node to the 'sortedInorder' list.
            sortedInorder.add(node.data);
            
            // Recursively traverse the right subtree.
            inorder(node.right, sortedInorder);
        }
    }

    // Public function to perform an inorder traversal starting from the root and store the result in a list.
    public void inorder(List<T> sortedInorder) {
        // Start the inorder traversal from the root of the tree.
        inorder(root, sortedInorder);
    }
}