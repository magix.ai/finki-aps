package v1;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SLLTree<E> implements Tree<E> {

    // SLLNode is the implementation of the Node interface
    class SLLNode<P> implements Tree.Node<P> {

        // Holds the links to the needed nodes
        SLLNode<P> parent, sibling, firstChild;

        // Hold the data
        P element;

        public SLLNode(P o) {
            element = o;
            parent = sibling = firstChild = null;
        }

        public P getElement() {
            return element;
        }

        public void setElement(P o) {
            element = o;
        }

    }

    protected SLLNode<E> root;

    public SLLTree() {
        root = null;
    }

    public Tree.Node<E> root() {
        return root;
    }

    public Tree.Node<E> parent(Tree.Node<E> node) {
        return ((SLLNode<E>) node).parent;
    }

    public int childCount(Tree.Node<E> node) {
        SLLNode<E> tmp = ((SLLNode<E>) node).firstChild;
        int num = 0;
        while (tmp != null) {
            tmp = tmp.sibling;
            num++;
        }
        return num;
    }

    public void makeRoot(E elem) {
        root = new SLLNode<E>(elem);
    }

    public Tree.Node<E> addChild(Tree.Node<E> node, E elem) {
        SLLNode<E> tmp = new SLLNode<E>(elem);
        SLLNode<E> curr = (SLLNode<E>) node;
        tmp.sibling = curr.firstChild;
        curr.firstChild = tmp;
        tmp.parent = curr;
        return tmp;
    }

    public void remove(Tree.Node<E> node) {
        SLLNode<E> curr = (SLLNode<E>) node;
        if (curr.parent != null) {
            if (curr.parent.firstChild == curr) {
                // The node is the first child of its parent
                // Reconnect the parent to the next sibling
                curr.parent.firstChild = curr.sibling;
            } else {
                // The node is not the first child of its parent
                // Start from the first and search the node in the sibling list
                // and remove it
                SLLNode<E> tmp = curr.parent.firstChild;
                while (tmp.sibling != curr) {
                    tmp = tmp.sibling;
                }
                tmp.sibling = curr.sibling;
            }
        } else {
            root = null;
        }
    }

    class SLLTreeIterator<T> implements Iterator<T> {

        SLLNode<T> start, current;

        public SLLTreeIterator(SLLNode<T> node) {
            start = node;
            current = node;
        }

        public boolean hasNext() {
            return (current != null);
        }

        public T next() throws NoSuchElementException {
            if (current != null) {
                SLLNode<T> tmp = current;
                current = current.sibling;
                return tmp.getElement();
            } else {
                throw new NoSuchElementException();
            }
        }

        public void remove() {
            if (current != null) {
                current = current.sibling;
            }
        }
    }

    public Iterator<E> children(Tree.Node<E> node) {
        return new SLLTreeIterator<E>(((SLLNode<E>) node).firstChild);
    }

    void printTreeRecursive(SLLNode<E> node, int level) {
        if (node == null)
            return;
        int i;
        SLLNode<E> tmp;

        for (i = 0; i < level; i++)
            System.out.print("  ");
        System.out.println(node.getElement().toString());
        tmp = ((SLLNode<E>) node).firstChild;
        while (tmp != null) {
            printTreeRecursive(tmp, level + 1);
            tmp = tmp.sibling;
        }
    }

    Tree.Node<E> searchRecursive(Tree.Node<E> node, E elem) {
        if (node.getElement().equals(elem))
            return node;
        Tree.Node<E> node_result=null;

        SLLNode<E> tmp_sybling = ((SLLNode<E>) node).sibling;
        if (tmp_sybling != null) {
            node_result = searchRecursive(tmp_sybling, elem);
        }
        if (node_result!=null){
            return node_result;
        }
        SLLNode<E> tmp_child = ((SLLNode<E>) node).firstChild;
        if (tmp_child != null) {
            node_result = searchRecursive(tmp_child, elem);
        }
        return node_result;
    }

    public void printTree() {
        printTreeRecursive(root, 0);
    }

    int printTreeRecursive_levels(Tree.Node<E> node, int level, String indent, String prefix, String additionalInfo, int reden_broj) {
        if (node == null)
            return 0;
        SLLNode<E> tmp;
        if (prefix.equals("")) {
            prefix = "1";
        }
//		if (additionalInfo.equals("")){additionalInfo="1";}
//		System.out.print(indent + prefix);
        System.out.println(Integer.toString(reden_broj + 1) + indent + prefix + " " + node.getElement().toString() + "    " + additionalInfo);
        tmp = ((SLLNode<E>) node).firstChild;
        int j = 1;
        int n = this.childCount(node);
        int childrenCount = 1;
        while (tmp != null) {
            int t = printTreeRecursive_levels(tmp, level + 1, indent + "  ", prefix + "." + j,
                    additionalInfo = "Braka/sestri po mene:" + Integer.toString(n - j), reden_broj + childrenCount);
            tmp = tmp.sibling;
            j++;
            childrenCount = childrenCount + t;
        }
        return childrenCount;
    }


    public void printTreeLevels() {
        printTreeRecursive_levels(root, 0, "", "", "", 0);
    }

    public int countMaxChildren() {
        return countMaxChildrenRecursive(root);
    }

    int countMaxChildrenRecursive(SLLNode<E> node) {
        int t = childCount(node);
        SLLNode<E> tmp = node.firstChild;
        while (tmp != null) {
            t = Math.max(t, countMaxChildrenRecursive(tmp));
            tmp = tmp.sibling;
        }
        return t;
    }

    public int getTreeDepth() {
        return getDepth(root, 0);
    }

    int getDepth(SLLNode<E> node, int currentDepth) {
        int t = 0;
        int t_min = -1;
        int t_max = 0;
        SLLNode<E> tmp = node.firstChild;
        while (tmp != null) {
            t = getDepth(tmp, 1);
            if (t_min < 0) {
                t_min = t;
            }
            if (t_max <= 0) {
                t_max = t;
            }
            t_min = Math.min(t_min, t);
            t_max = Math.max(t_max, t);
            tmp = tmp.sibling;
        }
        if (t_max - t_min > 1) {
            System.out.println("Drvtoto ne e balansirano. Konkretno grankata:");
            printTreeRecursive(node, currentDepth);
        }
        return currentDepth + t_max;
    }
}