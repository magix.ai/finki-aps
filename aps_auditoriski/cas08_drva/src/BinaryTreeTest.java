import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BinaryTreeTest {

    // Method to create an example integer-based binary tree.
    public static BinaryTree<Integer> GetExampleIntTree() {
        // Declare temporary TreeNode variables.
        TreeNode<Integer> tmp1, tmp2, tmp3;

        // Create a new BinaryTree of Integer type.
        BinaryTree<Integer> intTree = new BinaryTree<>();

        // Make the root of the tree with the value 1.
        intTree.makeRoot(1);

        // Add 7 as the left child of the root and keep a reference to this new node in tmp1.
        tmp1 = intTree.addLeftChild(7, intTree.root);

        // Add 2 as the left child of node 7 (tmp1) and keep a reference in tmp2.
        tmp2 = intTree.addLeftChild(2, tmp1);

        // Add 6 as the right child of node 7 (tmp1) and update the reference in tmp2.
        tmp2 = intTree.addRightChild(6, tmp1);

        // Add 5 as the left child of node 6 (tmp2) and keep a reference in tmp3.
        tmp3 = intTree.addLeftChild(5, tmp2);

        // Add 11 as the right child of node 6 (tmp2) and update the reference in tmp3.
        tmp3 = intTree.addRightChild(11, tmp2);

        // Add 9 as the right child of the root and keep a reference in tmp1.
        tmp1 = intTree.addRightChild(9, intTree.root);

        // Add 19 as the right child of node 9 (tmp1) and keep a reference in tmp2.
        tmp2 = intTree.addRightChild(19, tmp1);

        // Add 15 as the left child of node 19 (tmp2).
        tmp3 = intTree.addLeftChild(15, tmp2);

        // Return the example integer tree.
        return intTree;
    }

    // Method to create an example integer-based binary search tree.
    public static BinarySearchTree<Integer> GetExampleBSTree() {
        // Declare temporary TreeNode variables.
        TreeNode<Integer> tmp1, tmp2, tmp3;

        // Create a new BinaryTree of Integer type.
        BinarySearchTree<Integer> intTree = new BinarySearchTree<>();

        // Make the root of the tree with the value 1.
        intTree.makeRoot(8);

        // Add 2 as the left child of the root and keep a reference to this new node in tmp1.
        tmp1 = intTree.addLeftChild(2, intTree.root);

        // Add 1 as the left child of node 2 (tmp1) and keep a reference in tmp2.
        tmp2 = intTree.addLeftChild(1, tmp1);

        // Add 5 as the right child of node 2 (tmp1) and update the reference in tmp2.
        tmp2 = intTree.addRightChild(5, tmp1);

        // Add 3 as the left child of node 5 (tmp2) and keep a reference in tmp3.
        tmp3 = intTree.addLeftChild(3, tmp2);

        // Add 6 as the right child of node 5 (tmp2) and update the reference in tmp3.
        tmp3 = intTree.addRightChild(6, tmp2);

        // Add 11 as the right child of the root and keep a reference in tmp1.
        tmp1 = intTree.addRightChild(11, intTree.root);

        // Add 19 as the right child of node 11 (tmp1) and keep a reference in tmp2.
        tmp2 = intTree.addRightChild(19, tmp1);

        // Add 13 as the left child of node 19 (tmp2).
        tmp3 = intTree.addLeftChild(13, tmp2);

        // Return the example integer tree.
        return intTree;
    }

    // Method to create an example string-based binary tree.
    public static BinaryTree<String> GetExampleStringTree() {
        // Create a new BinaryTree of String type.
        BinaryTree<String> stringTree = new BinaryTree<>();

        // Set "apple" as the root of the tree.
        stringTree.makeRoot("apple");

        // Add "pear" as the left child of the root node.
        TreeNode<String> node1 = stringTree.addLeftChild("pear", stringTree.root);  // root

        // Add "peach" as the left child of the "pear" node.
        TreeNode<String> node2 = stringTree.addLeftChild("peach", node1);

        // Add "banana" as the right child of the root node.
        TreeNode<String> node3 = stringTree.addRightChild("banana", stringTree.root);

        // Add "orange" as the right child of the "banana" node.
        node2 = stringTree.addRightChild("orange", node3);

        // Add "lemon" as the left child of the "banana" node.
        node2 = stringTree.addLeftChild("lemon", node3);

        // Return the example string tree.
        return stringTree;
    }


    // method to test findNode()
    public static void exampleFindNode() {
        // Create an example integer tree and keep a reference in intTree.
        BinaryTree<Integer> intTree = GetExampleIntTree();

        // Create a Scanner to get user input.
        Scanner input = new Scanner(System.in);

        // Prompt the user to enter an integer value.
        System.out.print("Enter an integer value to search in the binary tree: ");

        // Read the user's input as an integer.
        int value = Integer.parseInt(input.next());

        // Call the findNode method to check if the value exists in the binary tree.
        if(intTree.findNode(value) != null)
            System.out.println("Node with value " + value + " exists in the given binary tree");
        else
            System.out.println("Node with value " + value + " does NOT exist in the given binary tree");
    }

    // Method to test the getLevel() method.
    public static void exampleGetLevel() {
        // Create an example integer tree and keep a reference in intTree.
        BinaryTree<Integer> intTree = GetExampleIntTree();

        // Create a Scanner to get user input.
        Scanner input = new Scanner(System.in);

        // Prompt the user to enter an integer value.
        System.out.print("Enter an integer value to get its level in the binary tree: ");

        // Read the user's input as an integer.
        int value = Integer.parseInt(input.next());

        // Call the getLevel method to find the level of the node in the binary tree.
        int level = intTree.getLevel(value);

        // Check if the level is not 0
        if (level != 0) {
            System.out.println("Level of " + value + " is " + level);
        } else {
            System.out.println(value + " is not present in the tree");
        }
    }

    // Method to test the getDist() method.
    public static void exampleGetDist() {
        // Create an example integer tree and keep a reference in intTree.
        BinaryTree<Integer> intTree = GetExampleIntTree();

        // Create a Scanner to get user input.
        Scanner input = new Scanner(System.in);

        // Prompt the user to enter an integer value.
        System.out.print("Enter two integer values to get their distance in the binary tree: ");

        // Read the user's input as an integer.
        int value1 = Integer.parseInt(input.next());
        int value2 = Integer.parseInt(input.next());

        // Call the getLevel method to find the level of the node in the binary tree.
        int dist = intTree.getDist(value1, value2);

        // Check if the distance is not 0
        if (dist != 0) {
            System.out.println("Dist("+ value1 + " , " + value2 + ") = " + dist);
        } else {
            System.out.println("Some of the values are not present in the tree");
        }
    }

    // Method to calculate the sum of all elements in a binary tree.
    public static int sumBT(TreeNode<Integer> node){
        // If the current node is null, return 0 (base case).
        if (node == null) 
            return 0; 
        
        // Recursively calculate the sum of the current node's data,
        // left subtree, and right subtree, and return the total sum.
        return (node.data + sumBT(node.left) + sumBT(node.right)); 
    }

    // Method to test the sumBT() method.
    public static void exampleSumBT(){
        // Create an example integer tree and keep a reference in intTree.
        BinaryTree<Integer> intTree = GetExampleIntTree();

        // Calculate the sum of all elements in the tree starting from the root.
        int sum = sumBT(intTree.root); 

        // Print the result.
        System.out.println("Sum of all the elements in the tree is: " + sum);
    }

    // This method calculates the sum of values in the left subtree of a binary tree
    // where each node's value is less than a specified 'value' parameter.
    // It recursively traverses the tree to sum the values.
    // - 'current': The current node being examined.
    // - 'value': The threshold value for nodes to be included in the sum.
    // Returns the sum of values in the left subtree meeting the condition.
    public static int sumMinLeftSubtree(TreeNode<Integer> current, int value) {
        // Base case: If the current node is null, return 0.
        if (current == null)
            return 0;
        // recursively summing the values in its subtrees
        int tmp = sumMinLeftSubtree(current.left, value) + sumMinLeftSubtree(current.right, value);
        // Check if the value of the current node is less than the specified 'value'.
        if (current.data < value) {
            // Include the current node's value in the sum and recursively sum
            return tmp + current.data;
        } else {
            // If the current node's value is not less than 'value', just return the subtree's values
            return tmp;
        }
    }
   
    // This method calculates the sum of values in the right subtree of a binary tree
    // where each node's value is greater than a specified 'value' parameter.
    // It recursively traverses the tree to sum the values.
    // - 'current': The current node being examined.
    // - 'value': The threshold value for nodes to be included in the sum.
    // Returns the sum of values in the right subtree meeting the condition.
    public static int sumMaxRightSubtree(TreeNode<Integer> current, int value) {
        // Base case: If the current node is null, return 0.
        if (current == null)
            return 0;
        // recursively summing the values in its subtrees
        int tmp = sumMaxRightSubtree(current.left, value) + sumMaxRightSubtree(current.right, value);
        // Check if the value of the current node is greater than the specified 'value'.
        if (current.data > value) {
            // Include the current node's value in the sum and recursively sum
            return current.data + tmp;
        } else {
            // If the current node's value is not less than 'value', just return the subtree's values
            return tmp;
        }
    }


    // Method to test the exampleSumSubtrees() method.
    public static void exampleSumSubtrees() {
        // Create an example integer tree and keep a reference in intTree.
        BinaryTree<Integer> intTree = GetExampleIntTree();

        // Create a Scanner to get user input.
        Scanner input = new Scanner(System.in);

        // Prompt the user to enter an integer value.
        System.out.print("Enter an integer value to search in the binary tree: ");

        // Read the user's input as an integer.
        int value = Integer.parseInt(input.next());

        // Find the node with the entered value in the binary tree.
        TreeNode<Integer> node = intTree.findNode(value);

        // Check if the node exists in the tree.
        if (node != null) {
            // Calculate and display the sum of the left subtree of the found node.
            System.out.println("The sum of the left subtree is " + sumMinLeftSubtree(node.left, value));
            
            // Calculate and display the sum of the right subtree of the found node.
            System.out.println("The sum of the right subtree is " + sumMaxRightSubtree(node.right, value));
        } else {
            // Display a message indicating that the node with the entered value doesn't exist in the tree.
            System.out.println("Node with value " + value + " does NOT exist in the given binary tree");
        }
    }

    // This method serves as an example for testing the 'isBalanced' method on a binary search tree (BST).
    public static void exampleIsBalanced() {
        // Create an example integer binary search tree (BST) and keep a reference in intTree.
        BinarySearchTree<Integer> intTree = GetExampleBSTree();

        // Print the contents of the binary search tree (BST).
        intTree.print();

        // Check if the binary search tree (BST) is balanced using the 'isBalanced' method.
        if (intTree.isBalanced(intTree.root)) {
            System.out.println("The given binary search tree is also balanced.");
        } else {
            System.out.println("The given binary search tree is NOT balanced.");
        }
    }


    // This method serves as an example for finding the kth smallest and kth largest elements in a binary search tree (BST).
    public static void exampleKthSmallest() {
        // Create an example integer binary search tree (BST) and keep a reference in intTree.
        BinarySearchTree<Integer> intTree = GetExampleBSTree();

        // Create a Scanner to get user input.
        Scanner input = new Scanner(System.in);

        // Read the user's input as an integer 'k' to find the kth smallest and kth largest elements.
        int k = Integer.parseInt(input.next());

        // Create a dynamic array to store the elements of the BST in sorted order.
        List<Integer> sortedInorder = new ArrayList<Integer>();

        // Perform an inorder traversal of the BST to populate 'sortedInorder' with sorted elements.
        intTree.inorder(sortedInorder);

        // Calculate the size of the 'sortedInorder' array.
        int len = sortedInorder.size();
        
        // Check if 'k' is within a valid range (1 <= k <= len).
        if (k > 0 && k <= len) {
            // Print the kth smallest and kth largest elements in the BST.
            System.out.println("The " + k + "th smallest element in BST is " + sortedInorder.get(k - 1));
            System.out.println("The " + k + "th biggest element in BST is " + sortedInorder.get(len - k));
        }
    }


    // Initialize the predecessor and successor nodes as global with null values.
    static TreeNode<Integer> pred = new TreeNode<Integer>(null);
    static TreeNode<Integer> succ = new TreeNode<Integer>(null);

    // This function finds the inorder predecessor and successor nodes of a given 'key' in a BST.
    static void findPredSucc(TreeNode<Integer> node, int key) {
        // Base case: If the current node is null, return.
        if (node == null) {
            return;
        }

        // If the current node's data matches the 'key', we've found the node.
        if (node.data == key) {
            // If the node has a left subtree, find the predecessor.
            if (node.left != null) {
                TreeNode<Integer> tmp = node.left;
                while (tmp.right != null)
                    tmp = tmp.right;
                    
                pred = tmp;
            }

            // If the node has a right subtree, find the successor.
            if (node.right != null) {
                TreeNode<Integer> tmp = node.right;
                while (tmp.left != null)
                    tmp = tmp.left;
                    
                succ = tmp;
            }
            return;
        }

        // If 'key' is smaller than the current node's data, search in the left subtree.
        if (node.data > key) {
            succ = node; // Update the successor.
            findPredSucc(node.left, key);
        }
        
        // If 'key' is larger than the current node's data, search in the right subtree.
        else {
            pred = node; // Update the predecessor.
            findPredSucc(node.right, key);
        }
    }

    // This method serves as an example for finding the inorder predecessor and successor of a key in a binary search tree (BST).
    public static void exampleInorderPredSucc() {
        // Create an example integer binary search tree (BST) and keep a reference in intTree.
        BinarySearchTree<Integer> intTree = GetExampleBSTree();

        // Create a Scanner to get user input.
        Scanner input = new Scanner(System.in);

        // Read the user's input as an integer 'key'.
        int key = Integer.parseInt(input.next());

        // Find the inorder predecessor and successor for the 'key' in the BST.
        findPredSucc(intTree.root, key);

        // Print the inorder predecessor if it exists.
        if (pred != null) {
            System.out.println("The inorder predecessor for " + key + " is " + pred.data);
        }

        // Print the inorder successor if it exists.
        if (succ != null) {
            System.out.println("The inorder successor for " + key + " is " + succ.data);
        }
    }


public static void main(String[] args) {
        
            //exampleFindNode();

            //exampleGetLevel();

            //exampleSumBT();

            //exampleSumSubtrees();

            //exampleGetDist();

            //exampleIsBalanced();

            //exampleKthSmallest();

            exampleInorderPredSucc();
            

    }

}