// Define an AVLNode class, extending TreeNode
public class AVLNode<T extends Comparable<T>> extends TreeNode<T>{
    int height;  // Additional field for AVLNode

    public AVLNode(T data) {
        super(data);
        height = 1;  // Initialize height as 1
    }
}
