import java.util.*;

public class Student implements Comparable<Student> {
    String Name;
    String Index;
    Integer Age;
    String Address;

    public Student(String name, String index, Integer age, String address) {
        this.Name = name;
        this.Index = index;
        this.Age = age;
        this.Address = address;
    }

    @Override
    public int compareTo(Student o) {
//        int cmp = this.Name.compareTo(o.Name);
        int cmp = -this.Name.compareTo(o.Name); // order by Age desc
        if (cmp != 0)
            return cmp;
//        cmp = this.Age.compareTo(o.Age);
        cmp = this.Age - o.Age;
//        cmp = o.Age - this.Age;  // order by Age descending
        if (cmp != 0) {
            return cmp;
        }
        cmp = this.Address.compareTo(o.Address); // order by Address asc
        if (cmp != 0) {
            return cmp;
        }
        cmp = this.Index.compareTo(o.Index); // order by Index asc
        return cmp;
    }

    @Override
    public String toString() {
        return "Name:" + this.Name + " Age:" + this.Age.toString() + " Address:" + this.Address + " Index:" + this.Index;
    }
}
