class Point {

    double x;
    double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }


    public double rastojanie(Point b) {
        return Math.sqrt((x - b.x) * (x - b.x) + (y - b.y) * (y - b.y));
//        return Math.sqrt((x - b.x) * (x - b.x) + (y - b.y) * (y - b.y) + (z - b.z) * (z - b.z));
    }

    public static double rastojanie2(Point a, Point b) {
        return a.rastojanie(b);
    }
}
