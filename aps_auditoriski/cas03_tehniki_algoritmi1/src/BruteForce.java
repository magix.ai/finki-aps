import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class BruteForce {

    int INF = 1000000;

    double min(double a, double b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }

    double najmalo_rastojanie(Point[] p, int n) {
/*
 Дадени се N точки (N >= 2) во дводимензионален простор. Пресметајте кое е најмалото растојание помеѓу две точки.
 Да се вратат и точките кои се најблиску помеѓу себе.
*/
        int i, j;
//        double best = INF;
//        int best_i = -1;
//        int best_j = -1;

//        Point a = p[0];
//        Point b = p[1];
        double best = p[0].rastojanie(p[1]);
        int best_i = 0;
        int best_j = 1;

        for (i = 0; i < (n - 1); i++) {
            for (j = i + 1; j < n; j++) {
//                best = min(best, rastojanie(p[i], p[j]));
//                best = Math.min(best, p[i].rastojanie(p[j]);
                double d = p[i].rastojanie(p[j]);
//                double d = Point.rastojanie1(p[i], p[j]);
                if (d < best) {
//                    a = p[i];
//                    b = p[j];
                    best_i = i;
                    best_j = j;
                    best = d;
                }
//                best = Math.min(best, Point.rastojanie1(p[i], p[j]));
            }
        }
//        System.out.println("Nablisku se tockite " + a + " i " + b + " i se oddaleceni" + best);
        System.out.println("Nablisku se tockite " + p[best_i] + " i " + p[best_j] + " i se oddaleceni" + best);
        return best;
    }

    int dali_se_napagaat(int i1, int j1, int i2, int j2) {
/*
Дадена е шаховска табла. Пресметајте на колку различни начини можат да се постават две “кралици”
без да се напаѓаат една со друга. Две “кралици” се напаѓаат ако се наоѓаат во ист ред, колона или дијагонала.
 */
        if (i1 == i2) // ista redica
        {
            return 1;
        }
        if (j1 == j2) // ista kolona
        {
            return 1;
        }
        if (Math.abs(i1 - i2) == Math.abs(j1 - j2)) // ista dijagonala
        {
            return 1;
        }
        return 0;
    }

    int broj_na_nacini() {
        int i1, j1, i2, j2;
        int rezultat = 0;
        int n =0;

        for (i1 = 0; i1 < 8; i1++) {
            for (j1 = 0; j1 < 8; j1++) {
                for (i2 = 0; i2 < 8; i2++) {
                    for (j2 = 0; j2 < 8; j2++) {
//                        System.out.println("Kralica 1 se naogja na " + i1 + "," + j1 + "Kralica 2 e na:" + i2 + "," + j2);
                        if (dali_se_napagaat(i1, j1, i2, j2) == 0) {
                            rezultat++;
                        }
                        n++;
                    }
//                    break;
                }
//                break;
            }
//            break;
        }
        System.out.println("Vkupno kombinacii:"+n);
        return rezultat;
    }

    int min_broj_moneti(int baran_iznos) {
        int m1, m2, m3, m4, m5;
        int dobien_iznos;
        int minimalen_broj_na_paricki = INF;
        int br_paricki;
        int[] moneti = new int[5];
//        int monteti50=0;
//        int monteti10=0;
//        int monteti5=0;
//        int monteti2=0;
//        int monteti1=0;
        int n=0;
        for (m1 = 0; m1 <= (baran_iznos / 50); m1++) {
            for (m2 = 0; m2 <= (baran_iznos / 10); m2++) {
                for (m3 = 0; m3 <= (baran_iznos / 5); m3++) {
                    for (m4 = 0; m4 <= (baran_iznos / 2); m4++) {
                        for (m5 = 0; m5 <= (baran_iznos / 1); m5++) {
                            dobien_iznos = m1 * 50 + m2 * 10 + m3 * 5 + m4 * 2 + m5 * 1;
                            if (dobien_iznos == baran_iznos) {
                                br_paricki = m1 + m2 + m3 + m4 + m5;
                                if (br_paricki < minimalen_broj_na_paricki) {
                                    moneti[0]=m1;
                                    moneti[1]=m2;
                                    moneti[2]=m3;
                                    moneti[3]=m4;
                                    moneti[4]=m5;
//                                    moneti50=m1;
//                                    moneti10=m2;
//                                    moneti5=m3;
//                                    moneti2=m4;
//                                    moneti1=m5;
                                    minimalen_broj_na_paricki = br_paricki;
                                }
                            }
                            n++;
                        }
                    }
                }
            }
        }
        System.out.println("Vkupno ispitani kombinacii:"+Integer.toString(n));
        System.out.println("50ki="+Integer.toString(moneti[0])+"; 10ki="+Integer.toString(moneti[1])+"; 5ki="+Integer.toString(moneti[2])+"; 2ki="+Integer.toString(moneti[3])+"; 1ci="+Integer.toString(moneti[4]));
        System.out.println("Moneti:"+Arrays.toString(moneti));
        return minimalen_broj_na_paricki;
    }

    // printf("%d\n", min_broj_moneti(79));
    public static void main(String[] args) throws IOException {

        BruteForce bf = new BruteForce();

//        Point[] a = new Point[10];
////
//        a[0] = new Point(4, 5.5);
////        System.out.println(a[0]);
//        a[1] = new Point(1.2, 12.3);
//        a[2] = new Point(7.9, 9.8);
//        a[3] = new Point(4.3, 5.5);
//        a[4] = new Point(-9.9, 8.2);
//        a[5] = new Point(1.2, 3);
//        a[6] = new Point(9, 2.4);
//        a[7] = new Point(2, 9.2);
//        a[8] = new Point(1, 5.6);
//        a[9] = new Point(2, 7);
//        System.out.println("Najmalo rastojanie: " + bf.najmalo_rastojanie(a, 10));
//        System.out.println("Broj na nacini: " + bf.broj_na_nacini());
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String text = br.readLine();
        System.out.println("Minimalen broj na moneti: " + bf.min_broj_moneti(Integer.parseInt(text)));
    }

}
