import java.util.Arrays;

public class Greedy {

    void sortiraj_paricki(int[] coins, int n) {
        int i, j, tmp;

        for (i = 0; i < n - 1; i++) {
            for (j = i + 1; j < n; j++) {
                if (coins[i] < coins[j]) {
                    tmp = coins[i];
                    coins[i] = coins[j];
                    coins[j] = tmp;
                }
            }
        }
    }

    // coins e niza so vrednostite na parickite koi se dadeni
// n e brojot na paricki
// sum e dadenata suma
// coinsNum e niza za resenieto so brojot na paricki od sekoja golemina na paricka
    int greedyCoins(int[] coins, int n, int sum, int[] coinsNum) {
        sortiraj_paricki(coins, n);

        int tekovna_moneta = 0;
        int br = 0; // vkupniot broj na paricki za da se formira dadenata suma

        while (sum > 0) {
            coinsNum[tekovna_moneta] = sum / coins[tekovna_moneta];
            System.out.println("Proveri ja monetata:" + Integer.toString(tekovna_moneta)+ " Vrednost:" + Integer.toString(coins[tekovna_moneta]) + " Zemi:" + Integer.toString(coinsNum[tekovna_moneta]) + " Preostanat iznos:"+Integer.toString(sum));
            sum -= coinsNum[tekovna_moneta] * coins[tekovna_moneta];
            br += coinsNum[tekovna_moneta];
            tekovna_moneta++;
        }

        while (tekovna_moneta < n) {
            coinsNum[tekovna_moneta] = 0;
            tekovna_moneta++;
        }
        System.out.println("Dostapni moneti:" + Arrays.toString(coins) + " Izbor:" + Arrays.toString(coinsNum));
        return br;
    }

    void sortiraj2(int[] p, int[] t, int n) {
        int i, j, tmpP, tmpT;

        for (i = 0; i < n - 1; i++) {
            for (j = i + 1; j < n; j++) {
                if (((float) p[i] / (float) t[i]) < ((float) p[j] / (float) t[j])) {
                    tmpP = p[i];
                    tmpT = t[i];
                    p[i] = p[j];
                    t[i] = t[j];
                    p[j] = tmpP;
                    t[j] = tmpT;
                }
            }
        }

    }

    void sortiraj(int[] p, float[] t, int n) {
        int i, j, tmpP;
        float tmpT;
//        int[] value_for_money;
//        for (i = 0; i < n; i++) {
//            value_for_money[i]=(float) p[i] / (float) t[i];
//        }
        for (i = 0; i < n - 1; i++) {
            for (j = i + 1; j < n; j++) {
                if (((float) p[i] / (float) t[i]) < ((float) p[j] / (float) t[j])) {
                    tmpP = p[i];
                    tmpT = t[i];
                    p[i] = p[j];
                    t[i] = t[j];
                    p[j] = tmpP;
                    t[j] = tmpT;
                }
            }
        }

    }
    // p i t gi sodrzat profitot i tezinata na objektite
    // C e kapacitet na paketot, x e vektor na resenieto
    float greedyFractionalKnapsack(int[] p, float[] t, float C, int n, float[] x) {

        sortiraj(p, t, n);
        // objektite se podredeni taka da bide zadovoleno p[i]/t[i] >= p[i+1]/t[i+1]
        System.out.println("Sortirani profiti:"+Arrays.toString(p));
        System.out.println("Sortirani tezini:"+Arrays.toString(t));
        int i;
        float profit = 0;

        for (i = 0; i < n; i++) {
            x[i] = 0;
        }

        for (i = 0; i < n; i++) {
            if (C > t[i]) {         // objektot go stavame celosno
                C -= t[i];
                profit += p[i];
                x[i] = 1;
            } else {                // objektot go stavame delumno
                float del_od_objektot = C / (float) t[i];
                profit += del_od_objektot * (float) p[i];
                x[i] = del_od_objektot;
                C = 0;
                break;
            }
        }
        System.out.println("Objektite ke bidat zemeni vo sledniot odnos:" + Arrays.toString(x));
        return profit;
    }

    public static void main(String[] args) {

        Greedy g = new Greedy();
//
//        int coins[] = new int[5];
//        int n = 5;
//        int sum = 310002;
//        int coinsNum[] = new int[5];
//
//        coins[0] = 1;
//        coins[1] = 2;
//        coins[2] = 5;
//        coins[3] = 10;
//        coins[4] = 50;
//
//        System.out.println("Greedy coins: " + g.greedyCoins(coins, n, sum, coinsNum));
//        System.out.println("Dostapni moneti:" + Arrays.toString(coins));
//        System.out.println("pocenta suma:" + sum);

        float C = 20;
        int n = 4;
        int p[] = new int[4];
        float t[] = new float[4];
        float vfm[] = new float[4];
        float x[] = new float[4];

//        p[0] = 25;
//        p[1] = 24;
//        p[2] = 15;
//
//        t[0] = 18;
//        t[1] = 15;
//        t[2] = 10;
        p[0] = 200;
        p[1] = 150;
        p[2] = 5;
        p[3] = 170;

        t[0] = 10;
        t[1] = 20;
        t[2] = (float) 0.5;
        t[3] = 5;

        for(int i =0; i<n; i++){
            vfm[i]=p[i]/t[i];
        }

        System.out.println("Greedy fractional knapsack: " + g.greedyFractionalKnapsack(p, t, C, n, x));

    }
}
