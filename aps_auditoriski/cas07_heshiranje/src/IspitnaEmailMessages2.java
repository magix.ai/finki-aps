import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Hashtable;

/*
По барање на својата компанија, Влатко направил едноставен систем за примање пораки.
Ги поделил маиловите во три категории: **UNREAD , READ и TRASH**.

    UNREAD: Пораки кои не се прочитани
    READ: Пораки кои се прочитани
    TRASH: Пораки избришани од корисникот

Системот е дизајниран така што повеќе пораки можат да имаат ист наслов и повеќе пораки може да бидат примени во даден момент,
но не може да се добие само една порака со ист наслов во ист момент. Притоа Влатко одлучил насловот, датумот и времето да ги чува како String.

Системот е базиран на неколку акции кои корисникот може да ги изврши и тоа:

    -да прочита порака така што таа од категорија UNREAD ќе стане READ (акција READ_MESSAGE),
    -да избрише порака која е или во UNREAD или во READ категорија така што категоријата ќе е TRASH (акција DELETE_MESSAGE)
    -да префрли порака од TRASH во READ (акција RESTORE_MESSAGE)
    -да префрли порака од READ во UNREAD (акција UNREAD_MESSAGE)

Секоја друга команда е невалидна и не треба да се извршува ништо.
За секоја порака треба да се зачуваат бројот на команди кои се применети врз неа. Почетно на секоја порака се извршени 0 команди.

Командите се во облик: <command> <message_heading> <message_date_time>
Пример на една команда: **READ_MESSAGE MESSAGE1 15.01.2020 17:15**

**Влез:**На влез прво се добива **N** кој ги означува бројот на пораки моментално во системот.
Потоа во секој ред се добива насловот на пораката, датумот и времето на примање на пораката и категоријата одделени со празно место.
Повеќе пораки може да имаат ист наслов.
Пример на една порака: **MESSAGE1 15.01.2020 17:15 READ**

Потоа на влез се добива **M** кој ги означува бројот на команди, па се добиваат командите кои треба да се извршат на пораките.
На крај се добива **P** кој означува бројот на пораки за кои треба да се испечати состојбата. Потоа на влез се примаат пораките - насловот, датумот и времето на примање на порака.

**Излез:** За секоја порака треба да се прикаже нејзината категорија и бројот на команди кои биле применети над пораката одделени со празно место.

**Пример:**

**Влез:**

    3
    MESSAGE1 15.01.2020 17:15 READ
    MESSAGE2 15.01.2020 18:15 READ
    MESSAGE1 15.01.2020 18:15 READ
    2
    UNREAD_MESSAGE MESSAGE1 15.01.2020 17:15
    DELETE_MESSAGE MESSAGE1 15.01.2020 18:15
    RESTORE_MESSAGE MESSAGE1 15.01.2020 18:15
    2
    MESSAGE1 15.01.2020 17:15
    MESSAGE1 15.01.2020 18:15

**Излез:**

    UNREAD 1
    READ 2

**Име на класа:** `SimpleEmailSystem`.

**Делумно решение:** Задачата се смета за делумно решена со најмалку 7 точни тест примери.

**Забелешка:** Дозволено е користење само на **ЕДНА** хеш мапа. **НЕ** е дозволено е користење на готови класи од Java API. Факторот на пополнетост на хеш табелата мора да биде **0.5**.

enum Category {
    READ,
    UNREAD,
    TRASH
}
*/
class EmailPoraka implements Comparable<EmailPoraka> {

    // A ChemicalElement object represents a chemical element.

    // Each element contains the two characters of the chemical symbol.
    // The first character must be an uppercase letter. Where present,
    // the second character must be a lowercase letter. If absent, the
    // second character is a space.

    private String datumVreme;
    private String naslov;
    private Category kategorija;
    private int Id;

    public EmailPoraka (String _datumVreme, String _naslov, Category _kategorija) {
        datumVreme=_datumVreme;
        naslov=_naslov;
        kategorija = _kategorija;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((dateTime == null) ? 0 : dateTime.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }


    @Override
    public int hashCode () {
//        return (datumVreme+naslov+Id.toString()).hashCode();
        return (datumVreme+naslov).hashCode();
    }


    public int stepCode () {
        return (sym2 == ' ') ? 1 : sym2 - 'a' + 2;
    }


    public String toString () {
        return naslov+":" + datumVreme+" "+kategorija;
    }

    public int compareTo (EmailPoraka that) {
//        EmailPoraka that = (EmailPoraka)other;
        if (that.datumVreme.compareTo(this.datumVreme)){return -1;}
        if (that.datumVreme>this.datumVreme){return 1;}
        if (that.naslov<this.naslov){return -1;}
        if (that.naslov>this.naslov){return 1;}
        return 0;
    }

    @Override
    public boolean equals (Object other) {
        EmailPoraka that = (EmailPoraka)other;
        return ((this.datumVreme.equals(that.datumVreme)) && (this.naslov.equals(that.naslov)) && (this.kategorija.equals(that.kategorija)));
    }
}