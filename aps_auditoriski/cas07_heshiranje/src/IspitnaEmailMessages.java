import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Hashtable;

enum Category {
    READ,
    UNREAD,
    TRASH
}

//class MapEntry<K extends Comparable<K>, E> implements Comparable<K> {
//
//    // Each MapEntry object is a pair consisting of a key (a Comparable
//    // object) and a value (an arbitrary object).
//    K key;
//    E value;
//
//    public MapEntry(K key, E val) {
//        this.key = key;
//        this.value = val;
//    }
//
//    public int compareTo(K that) {
//        // Compare this map entry to that map entry.
//        @SuppressWarnings("unchecked")
//        MapEntry<K, E> other = (MapEntry<K, E>) that;
//        return this.key.compareTo(other.key);
//    }
//
//    public String toString() {
//        return key + " " + value;
//    }
//}
//
//class SLLNode<E> {
//    protected E element;
//    protected SLLNode<E> succ;
//
//    public SLLNode(E elem, SLLNode<E> succ) {
//        this.element = elem;
//        this.succ = succ;
//    }
//
//    @Override
//    public String toString() {
//        return element.toString();
//    }
//}
//
//class CBHT<K extends Comparable<K>, E> {
//
//    // An object of class CBHT is a closed-bucket hash table, containing
//    // entries of class MapEntry.
//    private SLLNode<MapEntry<K, E>>[] buckets;
//
//    @SuppressWarnings("unchecked")
//    public CBHT(int m) {
//        // Construct an empty CBHT with m buckets.
//        buckets = (SLLNode<MapEntry<K, E>>[]) new SLLNode[m];
//    }
//
//    private int hash(K key) {
//        // Translate key to an index of the array buckets.
//        return Math.abs(key.hashCode()) % buckets.length;
//    }
//
//    public SLLNode<MapEntry<K, E>> search(K targetKey) {
//        // Find which if any node of this CBHT contains an entry whose key is
//        // equal
//        // to targetKey. Return a link to that node (or null if there is none).
//        int b = hash(targetKey);
//        for (SLLNode<MapEntry<K, E>> curr = buckets[b]; curr != null; curr = curr.succ) {
//            if (targetKey.equals(((MapEntry<K, E>) curr.element).key))
//                return curr;
//        }
//        return null;
//    }
//
//    public void insert(K key, E val) {        // Insert the entry <key, val> into this CBHT.
//        MapEntry<K, E> newEntry = new MapEntry<K, E>(key, val);
//        int b = hash(key);
//        for (SLLNode<MapEntry<K, E>> curr = buckets[b]; curr != null; curr = curr.succ) {
//            if (key.equals(((MapEntry<K, E>) curr.element).key)) {
//                // Make newEntry replace the existing entry ...
//                curr.element = newEntry;
//                return;
//            }
//        }
//        // Insert newEntry at the front of the 1WLL in bucket b ...
//        buckets[b] = new SLLNode<MapEntry<K, E>>(newEntry, buckets[b]);
//    }
//
//    public void delete(K key) {
//        // Delete the entry (if any) whose key is equal to key from this CBHT.
//        int b = hash(key);
//        for (SLLNode<MapEntry<K, E>> pred = null, curr = buckets[b]; curr != null; pred = curr, curr = curr.succ) {
//            if (key.equals(((MapEntry<K, E>) curr.element).key)) {
//                if (pred == null)
//                    buckets[b] = curr.succ;
//                else
//                    pred.succ = curr.succ;
//                return;
//            }
//        }
//    }
//
//    public String toString() {
//        String temp = "";
//        for (int i = 0; i < buckets.length; i++) {
//            temp += i + ":";
//            for (SLLNode<MapEntry<K, E>> curr = buckets[i]; curr != null; curr = curr.succ) {
//                temp += curr.element.toString() + " ";
//            }
//            temp += "\n";
//        }
//        return temp;
//    }
//
//}


class EmailMessage implements Comparable<EmailMessage> {
    public String subject;
    public String dateTime;
    public Category category;


    public EmailMessage(String subject, String dateTime) {
        this.subject = subject;
        this.dateTime = dateTime;
    }

    public EmailMessage(String subject, String dateTime, Category category) {
        this.subject = subject;
        this.dateTime = dateTime;
        this.category = category;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((dateTime == null) ? 0 : dateTime.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EmailMessage other = (EmailMessage) obj;
        if (dateTime == null) {
            if (other.dateTime != null)
                return false;
        } else if (!dateTime.equals(other.dateTime))
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        return true;
    }

    @Override
    public int compareTo(EmailMessage emailMessage) {

        if (this.dateTime.equals(emailMessage.dateTime) && this.category.equals(emailMessage.category))
            return 0;
        return 1;
    }

    @Override
    public String toString() {
        return subject + " " + category + " " + dateTime;
    }


}
/*

**Влез:**

3
MESSAGE1 15.01.2020 17:15 READ
MESSAGE2 15.01.2020 18:15 READ
MESSAGE1 15.01.2020 18:15 READ
3
UNREAD_MESSAGE MESSAGE1 15.01.2020 17:15
DELETE_MESSAGE MESSAGE1 15.01.2020 18:15
RESTORE_MESSAGE MESSAGE1 15.01.2020 18:15
2
MESSAGE1 15.01.2020 17:15
MESSAGE1 15.01.2020 18:15

**Излез:**

    UNREAD 1
    READ 2
 */
class SimpleEmailSystem {
    public static void main(String[] args) throws NumberFormatException, IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        CBHT<EmailMessage, Integer> cbht = new CBHT<>((int) (n / 0.5));
        EmailMessage email;
        String line;
        for (int i = 0; i < n; i++) {
            line = br.readLine();
            String[] parts = line.split("\\s+");
            email = new EmailMessage(parts[0], parts[1] + " " + parts[2], Category.valueOf(parts[3]));
            cbht.insert(email, 0);
        }
        String command;
        int numberOfCommands;
        int m = Integer.parseInt(br.readLine());
        for (int i = 0; i < m; i++) {
            line = br.readLine();
            String[] parts = line.split("\\s");
            command = parts[0];
            EmailMessage porakaStoSeMenuva=new EmailMessage(parts[1], parts[2] + " " + parts[3]);
            SLLNode<MapEntry<EmailMessage, Integer>> result = cbht.search(porakaStoSeMenuva);
            if (result == null) {
                continue;
            }

            email = result.element.key;
            numberOfCommands = result.element.value;
            if (command.equals("READ_MESSAGE") && email.category == Category.UNREAD) {
                email.category = Category.READ;
            } else if (command.equals("UNREAD_MESSAGE") && email.category == Category.READ) {
                email.category = Category.UNREAD;
            } else if (command.equals("RESTORE_MESSAGE") && email.category == Category.TRASH) {
                email.category = Category.READ;
            } else if (command.equals("DELETE_MESSAGE") && email.category != Category.TRASH) {
                email.category = Category.TRASH;
            } else {
                continue;
            }
            numberOfCommands++;
            cbht.insert(email, numberOfCommands);
        }
        int p = Integer.parseInt(br.readLine());
        for (int i = 0; i < p; i++) {
            line = br.readLine();
            String[] parts = line.split("\\s+");
            email = new EmailMessage(parts[0], parts[1] + " " + parts[2]);

            SLLNode<MapEntry<EmailMessage, Integer>> result = cbht.search(email);
            if (result != null) {
                System.out.println(result.element.key.category + " " + result.element.value);
            }

        }
    }


}

