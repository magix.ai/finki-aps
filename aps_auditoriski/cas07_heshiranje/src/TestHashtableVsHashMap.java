import java.util.Hashtable;
import java.util.HashMap;
import java.util.Map;
public class TestHashtableVsHashMap {
	public static void main(String[] args) {
		/*
1. HashMap is non synchronized. It is not-thread safe and can’t be shared between many threads without proper synchronization code whereas Hashtable is synchronized. It is thread-safe and can be shared with many threads.
2. HashMap allows one null key and multiple null values whereas Hashtable doesn’t allow any null key or value.
3. HashMap is generally preferred over HashTable if thread synchronization is not needed
		 */
		//----------hashtable -------------------------
		Hashtable<Integer,String> ht=new Hashtable<Integer,String>();
		ht.put(101,"AAP");
		ht.put(101,"APS");
		ht.put(102,"DB");
		ht.put(103,"AOK");
		System.out.println("-------------Hash table--------------");
		for (Map.Entry m:ht.entrySet()) {
			System.out.println(m.getKey()+" "+m.getValue());
		}

		//----------------hashmap--------------------------------
		HashMap<Integer,String> hm=new HashMap<Integer,String>();
		hm.put(100,"APS");
		hm.put(104,"AOK");
		hm.put(101,"KM");
		hm.put(102,"DB");
		hm.put(102,"BP");
		System.out.println("-----------Hash map-----------");
		for (Map.Entry m:hm.entrySet()) {
			System.out.println(m.getKey()+" "+m.getValue());
		}
	}
}
