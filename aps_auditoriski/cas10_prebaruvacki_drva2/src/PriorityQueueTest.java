
public class PriorityQueueTest {
    public static void main(String[] args) throws Exception {
        int i;
        
        Heap priorityQueue = new Heap(1000);

        int a[] = new int[]{1, 3, 2, 9, 8, 4, 10, 5, 7, 6};


        for (i=0;i<10;i++)
            priorityQueue.insert(a[i]);
        

        while (!priorityQueue.isEmpty())
            System.out.println(priorityQueue.removeMax());
        
    }
}
