import java.util.Iterator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.NoSuchElementException;

class SLLNode<E> {
    protected E element;
    protected SLLNode<E> succ;

    public SLLNode(E elem, SLLNode<E> succ) {
        this.element = elem;
        this.succ = succ;
    }

    @Override
    public String toString() {
        return element.toString();
    }
}


class SLL<E> {
    private SLLNode<E> first;

    public SLL() {
        // Construct an empty SLL
        this.first = null;
    }

    public void deleteList() {
        first = null;
    }

    public int length() {
        int ret;
        if (first != null) {
            SLLNode<E> tmp = first;
            ret = 1;
            while (tmp.succ != null) {
                tmp = tmp.succ;
                ret++;
            }
            return ret;
        } else
            return 0;

    }

    @Override
    public String toString() {
        String ret = new String();
        if (first != null) {
            SLLNode<E> tmp = first;
            ret += tmp + "->";
            while (tmp.succ != null) {
                tmp = tmp.succ;
                ret += tmp + "->";
            }
        } else
            ret = "Prazna lista!!!";
        return ret;
    }

    public void insertFirst(E o) {
        SLLNode<E> tmp = new SLLNode<E>(o, null);
        tmp.succ = first;
        //SLLNode<E> ins = new SLLNode<E>(o, first);
        first = tmp;
    }

    public void insertAfter(E o, SLLNode<E> node) {
        if (node != null) {
            SLLNode<E> ins = new SLLNode<E>(o, node.succ);
            node.succ = ins;
        } else {
            System.out.println("Dadenot jazol e null");
        }
    }

    public SLLNode<E> getPredecessor(E o, SLLNode<E> node) {
        if (first != null) {
            SLLNode<E> tmp = first;
            while (tmp.succ != node)
                tmp = tmp.succ;
            return tmp;
        } else {
            System.out.println("Listata e prazna");
            return null;
        }
    }

    public void insertBefore(E o, SLLNode<E> before) {

        if (first != null) {
            SLLNode<E> tmp = first;
            if (first == before) {
                this.insertFirst(o);
                return;
            }
            //ako first!=before
            while (tmp.succ != before)
                tmp = tmp.succ;
            if (tmp.succ == before) {
                SLLNode<E> ins = new SLLNode<E>(o, before);
                tmp.succ = ins;
            } else {
                System.out.println("Elementot ne postoi vo listata");
            }
        } else {
            System.out.println("Listata e prazna");
        }
    }

    public void insertLast(E o) {
        if (first != null) {
            SLLNode<E> tmp = first;
            while (tmp.succ != null)
                tmp = tmp.succ;
            SLLNode<E> ins = new SLLNode<E>(o, null);
            tmp.succ = ins;
        } else {
            insertFirst(o);
        }
    }

    public E deleteFirst() {
        if (first != null) {
            SLLNode<E> tmp = first;
            first = first.succ;
            return tmp.element;
        } else {
            System.out.println("Listata e prazna");
            return null;
        }
    }

    public E delete(SLLNode<E> node) {
        if (first != null) {
            SLLNode<E> tmp = first;
            if (first == node) {
                return this.deleteFirst();
            }
            while (tmp.succ != node && tmp.succ.succ != null)
                tmp = tmp.succ;
            if (tmp.succ == node) {
                tmp.succ = tmp.succ.succ;
                return node.element;
            } else {
                System.out.println("Elementot ne postoi vo listata");
                return null;
            }
        } else {
            System.out.println("Listata e prazna");
            return null;
        }

    }

    public SLLNode<E> getFirst() {
        return first;
    }

    public SLLNode<E> find(E o) {
        if (first != null) {
            SLLNode<E> tmp = first;
            while (tmp.element != o && tmp.succ != null)
                tmp = tmp.succ;
            if (tmp.element == o) {
                return tmp;
            } else {
                System.out.println("Elementot ne postoi vo listata");
            }
        } else {
            System.out.println("Listata e prazna");
        }
        return first;
    }

    public Iterator<E> iterator() {
        // Return an iterator that visits all elements of this list, in left-to-right order.
        return new LRIterator<E>();
    }

    // //////////Inner class ////////////

    private class LRIterator<E> implements Iterator<E> {

        private SLLNode<E> place, curr;

        private LRIterator() {
            place = (SLLNode<E>) first;
            curr = null;
        }

        public boolean hasNext() {
            return (place != null);
        }

        public E next() {
            if (place == null)
                throw new NoSuchElementException();
            E nextElem = place.element;
            curr = place;
            place = place.succ;
            return nextElem;
        }

        public void remove() {
            //Not implemented
        }
    }

    public void mirror() {
        if (first != null) {
            //m=nextsucc, p=tmp,q=next
            SLLNode<E> tmp = first;
            SLLNode<E> newsucc = null;
            SLLNode<E> next;

            while (tmp != null) {
                next = tmp.succ;
                tmp.succ = newsucc;
                newsucc = tmp;
                tmp = next;
            }
            first = newsucc;
        }

    }

    public void merge(SLL<E> in) {
        if (first != null) {
            SLLNode<E> tmp = first;
            while (tmp.succ != null)
                tmp = tmp.succ;
            tmp.succ = in.getFirst();
        } else {
            first = in.getFirst();
        }
    }
}

public class SpecialCharacterSLLJoin {
    public static boolean isSamoglaska(Character c) {
        return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
    }

    public static void join_lists_parallel(SLL<Character> lista1, SLL<Character> lista2) {
        SLL<Character> samoglaski = new SLL<Character>();
        SLL<Character> soglaski = new SLL<Character>();

        SLLNode<Character> j1 = lista1.getFirst();
        SLLNode<Character> j2 = lista2.getFirst();

        while (j1 != null && j2 != null) {
            if (isSamoglaska(j1.element))
                samoglaski.insertLast(j1.element);
            else {
                soglaski.insertLast(j1.element);
            }
            if (isSamoglaska(j2.element))
                samoglaski.insertLast(j2.element);
            else {
                soglaski.insertLast(j2.element);
            }
            j1 = j1.succ;
            j2 = j2.succ;
        }

        while (j1 != null) {
            if (isSamoglaska(j1.element))
                samoglaski.insertLast(j1.element);
            else {
                soglaski.insertLast(j1.element);
            }
            j1 = j1.succ;
        }

        while (j2 != null) {
            if (isSamoglaska(j2.element))
                samoglaski.insertLast(j2.element);
            else {
                soglaski.insertLast(j2.element);
            }
            j2 = j2.succ;
        }

        j1 = samoglaski.getFirst();
        System.out.println("Samoglaski:");
        System.out.println(samoglaski.toString());
//        while (j1 != null) {
//            System.out.print(j1.element);
//            if (j1.succ != null) {
//                System.out.print(" ");
//            }
//            j1 = j1.succ;
//        }
        System.out.println("\nSoglaski:");
//        j1 = soglaski.getFirst();
//        while (j1 != null) {
//            System.out.print(j1.element);
//            if (j1.succ != null) {
//                System.out.print(" ");
//            }
//            j1 = j1.succ;
//        }

        System.out.println(soglaski.toString());
    }

    public static void join_lists_sequential(SLL<Character> lista1, SLL<Character> lista2) {
        SLL<Character> samoglaski = new SLL<Character>();
        SLL<Character> soglaski = new SLL<Character>();

        SLLNode<Character> j1 = lista1.getFirst();
        SLLNode<Character> j2 = lista2.getFirst();

        while (j1 != null) {
            if (isSamoglaska(j1.element))
                samoglaski.insertLast(j1.element);
            else {
                soglaski.insertLast(j1.element);
            }
            j1 = j1.succ;
        }

        while (j2 != null) {
            if (isSamoglaska(j2.element))
                samoglaski.insertLast(j2.element);
            else {
                soglaski.insertLast(j2.element);
            }
            j2 = j2.succ;
        }

        j1 = samoglaski.getFirst();
        System.out.println("Samoglaski:");
        System.out.println(samoglaski.toString());
        System.out.println("\nSoglaski:");

        System.out.println(soglaski.toString());
    }

    //    Da se spojat dve listi od karakteri taka sto ke se dobijat dve rezulantni nizi, ednata so samoglaski, drugata so soglaski.
    public static void main(String[] args) throws IOException {
        SLL<Character> lista1 = new SLL<Character>();
        SLL<Character> lista2 = new SLL<Character>();
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
//        String s = stdin.readLine();
        String s = "qwertyuiop";
        for (int i = 0; i < s.length(); i++) {
            lista1.insertLast(s.charAt(i));
        }
//        s = stdin.readLine();
        s = "asdfghjkl";
        for (int i = 0; i < s.length(); i++) {
            lista2.insertLast(s.charAt(i));
        }
        System.out.println("Prvata lista:" + lista1.toString());
        System.out.println("Vtorata lista:" + lista2.toString());
        join_lists_parallel(lista1, lista2);
        join_lists_sequential(lista1, lista2);
    }

}
